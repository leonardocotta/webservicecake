<?php
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
CakePlugin::routes();

Router::mapResources('books');

Router::parseExtensions();

Router::connect(
 '/books/author::nomeauthor' ,
 array('controller' => 'books', 'action' => 'author'),
 array(
 'pass' => array('nomeauthor'),
 'nomeauthor' => '[a-zA-Z0-9_-]*',
 ));
Router::connect(
 '/books/title::nometitle' ,
 array('controller' => 'books', 'action' => 'title'),
 array(
 'pass' => array('nometitle'),
 'nometitle' => '[a-zA-Z0-9_-]*',
 ));

require CAKE . 'Config' . DS . 'routes.php';


/*Router::resourceMap(array(
    array('action' => 'index','method' => 'GET','id'=>false),
    array('action' => 'view','method' => 'GET','id'=>true),
    array('action' => 'add','method' => 'POST','id'=>false),
    array('action' => 'edit','method' => 'PUT','id'=>true),
    array('action' => 'delete','method' => 'DELETE','id'=>true),
    array('action' => 'update','method' => 'POST','id'=>true)
));*/
