<?php
class BooksController extends AppController {
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');
 
 
    public function index() {
        $books = $this->Book->find('all', array(
            'contain' => array(
            'title',
            'author',
            'isbn',
            'publisher'
        )));
        $this->set(array(
            'books' => $books
        ));
    }
 
    public function view($id) {
        $book = $this->Book->findById($id);
        $this->set(array(
            'book' => $book
        ));
    }
    public function author($author) {
        $books = $this->Book->find('first', 
            array("author" => "%$author%")
        );
        echo json_encode($books);
        exit(0);
    }
    public function title($title) {
        
        $books = $this->Book->find('first',array("title" => "%$title%") );
        echo json_encode($books);
        exit(0);
    }
    
}       